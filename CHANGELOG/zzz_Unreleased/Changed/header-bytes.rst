- Only read the first 4 KiB (by default) from code files rather than the entire
  file when searching for SPDX tags.  This speeds up the tool a bit.
